# generator-youbora-adapter [![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com) [![build](https://bitbucket-badges.atlassian.io/badge/npaw/generator-adapter-js.svg)](https://bitbucket.org/npaw/generator-adapter-js/) [![codecov](https://codecov.io/bb/npaw/generator-adapter-js/branch/master/graph/badge.svg)](https://codecov.io/bb/npaw/generator-adapter-js)
> Adapter generator for youboralib js V6

## Installation

First, install [Yeoman](http://yeoman.io) and generator-youbora-adapter using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-youbora-adapter
```

Then generate your new project:

```bash
yo youbora-adapter AdapterClassName
```

## License

MIT © [Nice People At Work](http://npaw.com)
