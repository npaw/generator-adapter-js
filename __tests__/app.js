var path = require('path')
var assert = require('yeoman-assert')
var helpers = require('yeoman-test')

describe('generator-youbora-adapter:app', () => {
  it('creates files with options', () => {
    return helpers.run(path.join(__dirname, '../generators/app'))
      .withPrompts({ authorLabel: 'Tester' })
      .withOptions({ name: 'TestClass' })
      .then(function () {
        assert.file([
          'src/adapter.js',
          'src/sp.js',
          'samples/mp4.html',
          '.gitignore',
          '.npmignore',
          '.eslintrc',
          'LICENSE',
          'CHANGELOG.md',
          'package.json',
          'README.md',
          'webpack.config.js'
        ])
      })
  })

  it('creates files with prompt', () => {
    return helpers.run(path.join(__dirname, '../generators/app'))
      .withPrompts({ authorLabel: 'Tester', name: 'Test' })
      .then(function () {
        assert.file([
          'LICENSE'
        ])
      })
  })
})
