var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.<%= adapterClass %> = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return null
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return null
  },

  /** Override to return Frames Per Second (FPS) */
  getFramesPerSecond: function () {
    return null
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return null
  },

  /** Override to return video duration */
  getDuration: function () {
    return null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return null
  },

  /** Override to return total downloaded bytes */
  getTotalBytes: function () {
    return null
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    return null
  },

  /** Override to return rendition */
  getRendition: function () {
    return null
  },

  /** Override to return title */
  getTitle: function () {
    return null
  },

  /** Override to return title2 */
  getTitle2: function () {
    return null
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return null
  },

  /** Override to return resource URL. */
  getResource: function () {
    return null
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return null
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return '<%= adapter %>'
  },

  /** Override to return the video latency */
  getLatency: function () {
    return null
  },

  /** Override to return the amount of packets sent */
  getPacketSent: function () {
    return null
  },

  /** Override to return the amount of packets lost */
  getPacketLoss: function () {
    return null
  },

  /** Override to return a json with metrics */
  getMetrics: function () {
    return null
  },

  /** Override to return the audio codec */
  getAudioCodec: function () {
    return null
  },

  /** Override to return the video codec */
  getVideoCodec: function () {
    return null
  },

  /** Override to return a chunk or intermediate manifest url */
  getURLToParse: function () {
    return null
  },

  /* ONLY ADS
   * -----------------------
   */

  // /** Override to return current ad position (only ads) */
  // getPosition: function () {
  //   return null
  // },

  // /** Override to return the given ad structure (list with number of pre, mid, and post breaks) (only ads) */
  // getGivenBreaks: function () {
  //   return null
  // },

  // /** Override to return the ad structure requested (list with number of pre, mid, and post breaks) (only ads) */
  // getExpectedBreaks: function () {
  //   return null
  // },

  // /** Override to return the structure of ads requested (only ads) */
  // getExpectedPattern: function () {
  //   return null
  // },

  // /** Override to return a list of playheads of ad breaks begin time (only ads) */
  // getBreaksTime: function () {
  //   return null
  // },

  // /** Override to return the number of ads given for the break (only ads) */
  // getGivenAds: function () {
  //   return null
  // },

  // /** Override to return the number of ads requested for the break (only ads) */
  // getExpectedAds: function () {
  //   return null
  // },

  // /** Override to return if the ad is being shown in the screen or not
  //  * The standard definition is: more than 50% of the pixels of the ad are on the screen
  //  * (only ads)
  //  */
  // getIsVisible: function () {
  //   return true
  // },

  // /** Override to return a boolean showing if the audio is enabled when the ad begins (only ads) */
  // getAudioEnabled: function () {
  //   return null
  // },

  // /** Override to return if the ad is skippable (only ads) */
  // getIsSkippable: function () {
  //   return null
  // },

  // /** Override to return a boolean showing if the player is in fullscreen mode when the ad begins (only ads) */
  // getIsFullscreen: function () {
  //   return null
  // },

  // /** Override to return the ad campaign (only ads) */
  // getCampaign: function () {
  //   return null
  // },

  // /** Override to return the ad creative id (only ads) */
  // getCreativeId: function () {
  //   return null
  // },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    // youbora.Util.logAllEvents(this.player)

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // References
    this.references = {
      'play': this.playListener.bind(this),
      'pause': this.pauseListener.bind(this),
      'playing': this.playingListener.bind(this),
      'error': this.errorListener.bind(this),
      'seeking': this.seekingListener.bind(this),
      'seeked': this.seekedListener.bind(this),
      'buffering': this.bufferingListener.bind(this),
      'buffered': this.bufferedListener.bind(this),
      'ended': this.endedListener.bind(this),
      'timeupdate': this.timeupdateListener.bind(this)
    }

    // Register listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.addEventListener(key, this.references[key])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
        delete this.references[key]
      }
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    var msg = null
    var code = null
    try {
      if (e && e.target && e.target.error) {
        code = e.target.error.code
        msg = e.target.error.message
      }
    } catch (err) {
      // nothing
    }
    this.fireError(code, msg)
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    this.fireSeekEnd()
  },

  /** Listener for 'buffering' event. */
  bufferingListener: function (e) {
    this.fireBufferBegin()
  },

  /** Listener for 'buffered' event. */
  bufferedListener: function (e) {
    this.fireBufferEnd()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  }
})

module.exports = youbora.adapters.<%= adapterClass %>
