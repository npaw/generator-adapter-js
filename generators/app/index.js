const Generator = require('yeoman-generator')
const chalk = require('chalk')
const yosay = require('yosay')
const path = require('path')

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts)

    this.argument('name', { type: String, required: false })
    this.argument('path', { type: String, required: false })
  }

  prompting () {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the mighty ' + chalk.red('Youbora Adapter') + ' generator!'
    ))

    let prompts = [{
      type: 'input',
      name: 'authorLabel',
      message: 'What\'s your author name?',
      store: true
    }]

    if (!this.options.name) {
      var folder = path.basename(process.cwd())
      folder = folder[0].toUpperCase() + folder.slice(1)
      prompts.push({
        type: 'input',
        name: 'name',
        message: 'What is the name of the adapter?',
        default: folder
      })
    }

    return this.prompt(prompts).then(props => {
      this.options.props = props
    })
  }

  writing () {
    var className = this.options.name || this.options.props.name
    var adapter = className.toLowerCase()
    var author = this.options.props.authorLabel

    this.fs.copyTpl(
      this.templatePath('.'),
      this.destinationPath('.'),
      {
        adapter: adapter,
        adapterClass: className,
        author: author
      }
    )

    this.fs.move(
      this.destinationPath('eslintrc'),
      this.destinationPath('.eslintrc')
    )

    this.fs.move(
      this.destinationPath('gitignore'),
      this.destinationPath('.gitignore')
    )

    this.fs.move(
      this.destinationPath('npmignore'),
      this.destinationPath('.npmignore')
    )
  }

  install () {
    this.npmInstall()
  }
}
